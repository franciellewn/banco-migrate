﻿using System;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
using ComponentFactory.Krypton.Workspace;
using BancoMigrate.Classes;
using System.Net;

namespace BancoMigrate
{
    public partial class Principal : KryptonForm
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void btnExecutar_Click(object sender, EventArgs e)
        {
            Parametros.Loja = numericLoja.Value.ToString();

            try
            {
                PrincipalAuxiliar principalAuxiliar = new PrincipalAuxiliar();

                if (comboTipo.Text == "PAF")
                {
                    Parametros.TipoLoja = 'P';
                    principalAuxiliar.ExecutarTrilhaPaf(barraProgresso, labelExecucao);
                    MessageBox.Show("Procedimento executado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (comboTipo.Text == "NFCE")
                {
                    Parametros.TipoLoja = 'N';
                    principalAuxiliar.ExecutarTrilhaNfce(barraProgresso, labelExecucao);
                    MessageBox.Show("Procedimento executado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (comboTipo.Text == "NFCE/S@T")
                {
                    Parametros.TipoLoja = 'S';
                    principalAuxiliar.ExecutarTrilhaNfceSat(barraProgresso, labelExecucao);
                    MessageBox.Show("Procedimento executado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    throw new Exception("Nenhum tipo de loja foi selecionado");
                }
            }
            catch (Exception ex)
            {
                labelExecucao.AppendText("Execução abortada por motivo de falha na rotina: " + ex.Message);
                MessageBox.Show(ex.Message, "Atenção" , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public string DescorbrirLoja()
        {
            try
            {
                string loja = "";
                string maquina = Dns.GetHostName();
                IPAddress[] ips = Dns.GetHostAddresses(maquina);
                string ip = ips[1].ToString();
                if (ip.Length == 10)
                {
                    loja = (ip.Substring(7, 1));
                }
                else if (ip.Length == 11)
                {
                    loja = (ip.Substring(7, 2));
                }
                else if (ip.Length == 12)
                {
                    loja = (ip.Substring(7, 3));
                }
                else
                {
                    loja = String.Empty;
                }
                return loja;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            try
            {
                string ip = DescorbrirLoja();
                if (!ip.Equals(String.Empty))
                {
                    numericLoja.Value = Convert.ToInt32(ip);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        private void botaoSenha_Click(object sender, EventArgs e)
        {
            if (labelSenha.Text.Equals("dns456!@#09"))
            {
                numericLoja.Enabled = true;
            }
            else
            {
                MessageBox.Show("Senha incorreta", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
