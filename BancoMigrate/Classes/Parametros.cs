﻿namespace BancoMigrate.Classes
{
    public static class Parametros
    {
        public static string Loja
        {
            get;
            set;
        }

        public static char TipoLoja
        {
            get;
            set;
        }
    }
}
