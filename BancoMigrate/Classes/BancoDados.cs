﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Xml;

namespace BancoMigrate.Classes
{
    public class BancoDados
    {
        public void FecharProcessosUsamBanco()
        {
            try
            {
                foreach (Process processo in Process.GetProcesses())
                {
                    if (processo.ProcessName.Equals("ComSefaz") || processo.ProcessName.Equals("FrenteLojaNfce") || processo.ProcessName.Equals("PDV_Damyller"))
                    {
                        processo.Kill();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SqlConnection CriarConexaoSqlServerSemBanco()
        {
            try
            {
                SqlConnection conexao = new SqlConnection("Data Source = LOCALHOST\\SQLEXPRESS; User ID = sa");
                return conexao;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SqlConnection CriarConexaoSqlServerComBanco()
        {
            try
            {
                SqlConnection conexao = new SqlConnection("Data Source = LOCALHOST\\SQLEXPRESS; Initial Catalog = FL_DAMY; User ID = sa");
                return conexao;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DesanexarBancoDados()
        {
            SqlConnection novaConexao = null;
            try
            {
                novaConexao = CriarConexaoSqlServerComBanco();
                novaConexao.Open();
                SqlCommand comandoDesanexar = new SqlCommand(@"USE [master]
                                                               ALTER DATABASE[FL_DAMY] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
                                                               USE[master]
                                                               EXEC master.dbo.sp_detach_db @dbname = N'FL_DAMY'", novaConexao);
                comandoDesanexar.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (novaConexao != null && novaConexao.State == System.Data.ConnectionState.Open)
                {
                    novaConexao.Close();
                }
            }
        }

        public void AnexarBancoDados()
        {
            SqlConnection novaConexao = null;
            try
            {
                novaConexao = CriarConexaoSqlServerSemBanco();
                novaConexao.Open();
                SqlCommand comandoAnexar = new SqlCommand(@"RESTORE DATABASE FL_DAMY from disk = 'C:\Backup\Backup-FL_DAMY.BAK' WITH REPLACE", novaConexao);
                comandoAnexar.CommandTimeout = 180;
                comandoAnexar.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (novaConexao != null && novaConexao.State == System.Data.ConnectionState.Open)
                {
                    novaConexao.Close();
                }
            }
        }

        public void AlterarIpConfigFrenteLoja()
        {
            try
            {
                if (Parametros.TipoLoja.Equals('P'))
                {
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(@"C:\FrenteLoja\PDV_Damyller.exe.Config");
                    xmlConfig.SelectNodes("configuration/connectionStrings/add").Item(0).Attributes["connectionString"].Value = Criptografa("Data Source=172.16.*.2\\SQLEXPRESS;Initial Catalog=FL_DAMY;Persist Security Info=True;Connect Timeout=600;User Id=sa;APP=PDV".Replace("*", Parametros.Loja));
                    xmlConfig.Save(@"C:\FrenteLoja\PDV_Damyller.exe.Config");
                }
                else if (Parametros.TipoLoja.Equals('N'))
                {
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(@"C:\FrenteLojaNfce\FrenteLojaNfce.exe.Config");
                    xmlConfig.SelectNodes("configuration/connectionStrings/add").Item(0).Attributes["connectionString"].Value = Criptografa("Data Source=172.16.*.2\\SQLEXPRESS;Initial Catalog=FL_DAMY;Persist Security Info=True;Connect Timeout=180;User Id=sa".Replace("*", Parametros.Loja));
                    xmlConfig.Save(@"C:\FrenteLojaNfce\FrenteLojaNfce.exe.Config");
                }
                else if (Parametros.TipoLoja.Equals('S'))
                {
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(@"C:\FrenteLojaNfce\FrenteLojaNfce.exe.Config");
                    xmlConfig.SelectNodes("configuration/connectionStrings/add").Item(0).Attributes["connectionString"].Value = Criptografa("Data Source=172.16.*.2\\SQLEXPRESS;Initial Catalog=FL_DAMY;Persist Security Info=True;Connect Timeout=180;User Id=sa".Replace("*", Parametros.Loja));
                    xmlConfig.Save(@"C:\FrenteLojaNfce\FrenteLojaNfce.exe.Config");
                }
                else
                {
                    throw new Exception("O tipo de loja informado não existe");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AlterarIpConfigComSefaz()
        {
            try
            {
                XmlDocument xmlConfig = new XmlDocument();
                xmlConfig.Load(@"C:\ComSefaz\ComSefaz.exe.Config");
                xmlConfig.SelectNodes("configuration/connectionStrings/add").Item(0).Attributes["connectionString"].Value = Criptografa("Data Source=172.16.*.2\\SQLEXPRESS;Initial Catalog=FL_DAMY;Persist Security Info=True;Connect Timeout=180;User Id=sa".Replace("*", Parametros.Loja));
                xmlConfig.Save(@"C:\ComSefaz\ComSefaz.exe.Config");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Descriptografa(string texto)
        {
            Byte[] b = Convert.FromBase64String(texto);
            string decrip = System.Text.ASCIIEncoding.ASCII.GetString(b);
            return decrip;
        }

        public string Criptografa(string texto)
        {
            Byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(texto);
            string cript = Convert.ToBase64String(b);
            return cript;
        }

    }
}
