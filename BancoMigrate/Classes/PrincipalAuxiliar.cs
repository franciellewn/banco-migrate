﻿using System;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using BancoMigrate.Classes;
using System.Net;
using ComponentFactory.Krypton.Toolkit;

namespace BancoMigrate.Classes
{
    public class PrincipalAuxiliar
    {
        BancoDados bancoDados = new BancoDados();
        Utilitarios utilitarios = new Utilitarios();

        public void ExecutarTrilhaPaf(ProgressBar barraProgresso, KryptonRichTextBox labelExecucao)
        {
            try
            {
                LimparLabelExecucao(labelExecucao);
                MostrarEtapaExecucaoLabel("Iniciando...", labelExecucao);
                LimparBarraProgresso(barraProgresso);
                MostrarEtapaExecucaoLabel("Fechando os processos que usam o banco de dados", labelExecucao);
                bancoDados.FecharProcessosUsamBanco();
                IncrementarBarraProgress(40, barraProgresso);
                MostrarEtapaExecucaoLabel("Anexando o banco de dados", labelExecucao);
                bancoDados.AnexarBancoDados();
                IncrementarBarraProgress(40, barraProgresso);
                MostrarEtapaExecucaoLabel("Inserindo o ip da loja no config do FrenteLoja", labelExecucao);
                bancoDados.AlterarIpConfigFrenteLoja();
                IncrementarBarraProgress(20, barraProgresso);
                MostrarEtapaExecucaoLabel("Execução concluída", labelExecucao);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ExecutarTrilhaNfce(ProgressBar barraProgresso, KryptonRichTextBox labelExecucao)
        {
            try
            {
                LimparLabelExecucao(labelExecucao);
                MostrarEtapaExecucaoLabel("Iniciando...", labelExecucao);
                LimparBarraProgresso(barraProgresso);
                MostrarEtapaExecucaoLabel("Fechando os processos que usam o banco de dados", labelExecucao);
                bancoDados.FecharProcessosUsamBanco();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Anexando o banco de dados", labelExecucao);
                bancoDados.AnexarBancoDados();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Criando a pasta XML", labelExecucao);
                utilitarios.CriaPastaXml();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Solicitando forcener manualmente permissões para a pasta XML", labelExecucao);
                utilitarios.SolicitarLiberarPermissaoPastaXML();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Mapeando unidade de rede", labelExecucao);
                utilitarios.MapearUnidadeRede("", Dns.GetHostName() + "\\Administrador");
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Criando a pasta ComSefaz", labelExecucao);
                utilitarios.CriarPastaComSefaz();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Copiando os arquivos para a pasta ComSefaz", labelExecucao);
                utilitarios.CopiarArquivosComSefaz();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Inserindo o ip da loja no config do FrenteLojaNfce", labelExecucao);
                bancoDados.AlterarIpConfigFrenteLoja();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Inserindo o ip da loja no config do ComSefaz", labelExecucao);
                bancoDados.AlterarIpConfigComSefaz();
                IncrementarBarraProgress(9 ,barraProgresso);
                MostrarEtapaExecucaoLabel("Limpando a pasta inicializar do Windows", labelExecucao);
                utilitarios.LimparPastaInicializarWindows();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Criando o atalho do ComSefaz no inicializar", labelExecucao);
                utilitarios.CriarAtalhoComSefaz();
                IncrementarBarraProgress(10, barraProgresso);
                MostrarEtapaExecucaoLabel("Execução concluída", labelExecucao);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ExecutarTrilhaNfceSat(ProgressBar barraProgresso, KryptonRichTextBox labelExecucao)
        {
            try
            {
                LimparLabelExecucao(labelExecucao);
                MostrarEtapaExecucaoLabel("Iniciando...", labelExecucao);
                LimparBarraProgresso(barraProgresso);
                MostrarEtapaExecucaoLabel("Fechando os processos que usam o banco de dados", labelExecucao);
                bancoDados.FecharProcessosUsamBanco();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Anexando o banco de dados", labelExecucao);
                bancoDados.AnexarBancoDados();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Criando a pasta XML", labelExecucao);
                utilitarios.CriaPastaXml();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Solicitando forcener manualmente permissões para a pasta XML", labelExecucao);
                utilitarios.SolicitarLiberarPermissaoPastaXML();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Mapeando unidade de rede", labelExecucao);
                utilitarios.MapearUnidadeRede("", Dns.GetHostName() + "\\Administrador");
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Criando a pasta ComSefaz", labelExecucao);
                utilitarios.CriarPastaComSefaz();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Copiando os arquivos para a pasta ComSefaz S@T", labelExecucao);
                utilitarios.CopiarArquivosComSefazSat();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Inserindo o ip da loja no config do FrenteLojaNfce", labelExecucao);
                bancoDados.AlterarIpConfigFrenteLoja();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Inserindo o ip da loja no config do ComSefaz", labelExecucao);
                bancoDados.AlterarIpConfigComSefaz();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Limpando a pasta inicializar do Windows", labelExecucao);
                utilitarios.LimparPastaInicializarWindows();
                IncrementarBarraProgress(9, barraProgresso);
                MostrarEtapaExecucaoLabel("Criando o atalho do ComSefaz no inicializar", labelExecucao);
                utilitarios.CriarAtalhoComSefaz();
                IncrementarBarraProgress(10, barraProgresso);
                MostrarEtapaExecucaoLabel("Execução concluída", labelExecucao);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void IncrementarBarraProgress(int porcentagem, ProgressBar barraProgresso)
        {
            barraProgresso.Value += porcentagem;
        }

        public void LimparBarraProgresso(ProgressBar barraProgresso)
        {
            barraProgresso.Value = 0;
        }

        public void MostrarEtapaExecucaoLabel(string texto, KryptonRichTextBox labelExecucao)
        {
            labelExecucao.AppendText(texto + "\n");
            labelExecucao.ScrollToCaret();
        }

        public void LimparLabelExecucao(KryptonRichTextBox labelExecucao)
        {
            labelExecucao.Clear();
        }
    }
}
