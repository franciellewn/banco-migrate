﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Security;
using System.Runtime.InteropServices;
using System.Net;

namespace BancoMigrate.Classes
{
    public class Utilitarios
    {
        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2A(ref structNetResource pstNetRes, string psPassword, string psUsername, int piFlags);
        [DllImport("mpr.dll")]
        private static extern int WNetCancelConnection2A(string psName, int piFlags, int pfForce);
        [DllImport("mpr.dll")]
        private static extern int WNetConnectionDialog(int phWnd, int piType);
        [DllImport("mpr.dll")]
        private static extern int WNetDisconnectDialog(int phWnd, int piType);
        [DllImport("mpr.dll")]
        private static extern int WNetRestoreConnectionW(int phWnd, string psLocalDrive);

        private const int RESOURCETYPE_DISK = 0x1;
        private const int CONNECT_INTERACTIVE = 0x00000008;
        private const int CONNECT_PROMPT = 0x00000010;
        private const int CONNECT_UPDATE_PROFILE = 0x00000001;
        private const int CONNECT_REDIRECT = 0x00000080;
        private const int CONNECT_COMMANDLINE = 0x00000800;
        private const int CONNECT_CMD_SAVECRED = 0x00001000;

        private struct structNetResource
        {
            public int iScope;
            public int iType;
            public int iDisplayType;
            public int iUsage;
            public string sLocalName;
            public string sRemoteName;
            public string sComment;
            public string sProvider;
        }

        public void CriarPastaComSefaz()
        {
            try
            {
                if (!Directory.Exists(@"C:/ComSefaz"))
                {
                    Directory.CreateDirectory(@"C:/ComSefaz");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CopiarArquivosComSefaz()
        {
            try
            {
                if (Parametros.TipoLoja.Equals('N'))
                {
                    Thread.Sleep(5000);
                    DirectoryInfo dirComSefazNfce = new DirectoryInfo("ComSefazNfce");
                    foreach (FileInfo arquivo in dirComSefazNfce.GetFiles())
                    {
                        File.Copy(arquivo.FullName, @"C:/ComSefaz/" + arquivo.Name, true);
                    }
                }
                else
                {
                    throw new Exception("O tipo de loja informado não existe");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CopiarArquivosComSefazSat()
        {
            DirectoryInfo dirComSefazSat = null;
            try
            {
                if (Parametros.TipoLoja.Equals('S'))
                {
                    switch (Parametros.Loja)
                    {
                        case "28":  dirComSefazSat = new DirectoryInfo("ComSefazSat28"); break;
                        case "31":  dirComSefazSat = new DirectoryInfo("ComSefazSat31"); break;
                        case "39":  dirComSefazSat = new DirectoryInfo("ComSefazSat39"); break;
                        case "40":  dirComSefazSat = new DirectoryInfo("ComSefazSat40"); break;
                        case "42":  dirComSefazSat = new DirectoryInfo("ComSefazSat42"); break;
                        case "45":  dirComSefazSat = new DirectoryInfo("ComSefazSat45"); break;
                        case "53":  dirComSefazSat = new DirectoryInfo("ComSefazSat53"); break;
                        case "109": dirComSefazSat = new DirectoryInfo("ComSefazSat109"); break;
                        case "133": dirComSefazSat = new DirectoryInfo("ComSefazSat133"); break;
                    }
                    foreach (FileInfo arquivo in dirComSefazSat.GetFiles())
                    {
                        File.Copy(arquivo.FullName, @"C:/ComSefaz/" + arquivo.Name, true);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void LimparPastaInicializarWindows()
        {
            try
            {
                string caminhoPastaInicializar = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Windows\Start Menu\Programs\Startup";
                DirectoryInfo pastaInicializar = new DirectoryInfo(caminhoPastaInicializar);
                foreach (FileInfo atalho in pastaInicializar.GetFiles())
                {
                    File.Delete(atalho.FullName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CriarAtalhoComSefaz()
        {
            try
            {
                File.Copy("ComSefaz.exe - Atalho.lnk", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Windows\Start Menu\Programs\Startup\ComSefaz.exe - Atalho.lnk", true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CriaPastaXml()
        {
            try
            {
                if (!Directory.Exists("C:\\XML"))
                {
                    Directory.CreateDirectory("C:\\XML");
                }
                if (!Directory.Exists("C:\\XML\\XML"))
                {
                    Directory.CreateDirectory("C:\\XML\\XML");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SolicitarLiberarPermissaoPastaXML()
        {
            MessageBox.Show("Para o funcionamento correto do processo, você agora precisa verificar as permissões de acesso e compartilhameto da pasta XML, você verificou as informações?", "Processo manual", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
        }

        public void MapearUnidadeRede(string usuario, string senha)
        {
            try
            {
                structNetResource estruturaRede = new structNetResource();
                estruturaRede.iScope = 2;
                estruturaRede.iType = RESOURCETYPE_DISK;
                estruturaRede.iDisplayType = 3;
                estruturaRede.iUsage = 1;
                estruturaRede.sRemoteName = "\\\\" + Dns.GetHostName() + "\\XML";
                estruturaRede.sLocalName = "X:";
                int iFlags = 0;
                iFlags += CONNECT_INTERACTIVE;
                iFlags += CONNECT_CMD_SAVECRED;
                iFlags += CONNECT_UPDATE_PROFILE; 
                RemoverUnidadeRede();
                int i = WNetAddConnection2A(ref estruturaRede,usuario, senha, iFlags);
                if (i > 0)
                {
                    throw new System.ComponentModel.Win32Exception(i);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RemoverUnidadeRede()
        {
            try
            {
                int iFlags = 0;
                iFlags += CONNECT_UPDATE_PROFILE;
                int i = WNetCancelConnection2A("X:", iFlags, 1);
                if (i != 0)
                {
                    i = WNetCancelConnection2A("X:", iFlags, 1);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
