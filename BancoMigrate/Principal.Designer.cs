﻿namespace BancoMigrate
{
    partial class Principal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.btnExecutar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.barraProgresso = new System.Windows.Forms.ProgressBar();
            this.labelTipo = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.comboTipo = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.numericLoja = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.labelExecucao = new ComponentFactory.Krypton.Toolkit.KryptonRichTextBox();
            this.labelSenha = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.botaoSenha = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            ((System.ComponentModel.ISupportInitialize)(this.comboTipo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExecutar
            // 
            this.btnExecutar.Location = new System.Drawing.Point(153, 263);
            this.btnExecutar.Name = "btnExecutar";
            this.btnExecutar.Size = new System.Drawing.Size(121, 25);
            this.btnExecutar.TabIndex = 5;
            this.btnExecutar.Values.Text = "Executar";
            this.btnExecutar.Click += new System.EventHandler(this.btnExecutar_Click);
            // 
            // barraProgresso
            // 
            this.barraProgresso.Location = new System.Drawing.Point(3, 69);
            this.barraProgresso.Name = "barraProgresso";
            this.barraProgresso.Size = new System.Drawing.Size(425, 23);
            this.barraProgresso.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.barraProgresso.TabIndex = 6;
            // 
            // labelTipo
            // 
            this.labelTipo.Location = new System.Drawing.Point(2, 3);
            this.labelTipo.Name = "labelTipo";
            this.labelTipo.Size = new System.Drawing.Size(37, 20);
            this.labelTipo.TabIndex = 10;
            this.labelTipo.Values.Text = "Tipo:";
            // 
            // comboTipo
            // 
            this.comboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipo.DropDownWidth = 72;
            this.comboTipo.Items.AddRange(new object[] {
            "PAF",
            "NFCE",
            "NFCE/S@T"});
            this.comboTipo.Location = new System.Drawing.Point(42, 4);
            this.comboTipo.Name = "comboTipo";
            this.comboTipo.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.comboTipo.Size = new System.Drawing.Size(90, 21);
            this.comboTipo.TabIndex = 11;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(3, 34);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(36, 20);
            this.kryptonLabel1.TabIndex = 13;
            this.kryptonLabel1.Values.Text = "Loja:";
            // 
            // numericLoja
            // 
            this.numericLoja.Enabled = false;
            this.numericLoja.Location = new System.Drawing.Point(42, 35);
            this.numericLoja.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericLoja.Name = "numericLoja";
            this.numericLoja.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.numericLoja.Size = new System.Drawing.Size(90, 22);
            this.numericLoja.TabIndex = 14;
            this.numericLoja.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelExecucao
            // 
            this.labelExecucao.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelExecucao.Location = new System.Drawing.Point(3, 98);
            this.labelExecucao.Name = "labelExecucao";
            this.labelExecucao.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.labelExecucao.Size = new System.Drawing.Size(425, 160);
            this.labelExecucao.TabIndex = 15;
            this.labelExecucao.Text = "";
            // 
            // labelSenha
            // 
            this.labelSenha.Location = new System.Drawing.Point(138, 36);
            this.labelSenha.Name = "labelSenha";
            this.labelSenha.PasswordChar = '*';
            this.labelSenha.Size = new System.Drawing.Size(165, 23);
            this.labelSenha.TabIndex = 16;
            // 
            // botaoSenha
            // 
            this.botaoSenha.Location = new System.Drawing.Point(309, 27);
            this.botaoSenha.Name = "botaoSenha";
            this.botaoSenha.Size = new System.Drawing.Size(119, 36);
            this.botaoSenha.TabIndex = 17;
            this.botaoSenha.Values.Image = global::BancoMigrate.Properties.Resources.senha;
            this.botaoSenha.Values.Text = "Desbloquear";
            this.botaoSenha.Click += new System.EventHandler(this.botaoSenha_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 292);
            this.Controls.Add(this.botaoSenha);
            this.Controls.Add(this.labelSenha);
            this.Controls.Add(this.labelExecucao);
            this.Controls.Add(this.numericLoja);
            this.Controls.Add(this.kryptonLabel1);
            this.Controls.Add(this.comboTipo);
            this.Controls.Add(this.labelTipo);
            this.Controls.Add(this.barraProgresso);
            this.Controls.Add(this.btnExecutar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Principal";
            this.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Blue;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alternar banco de dados";
            this.Load += new System.EventHandler(this.Principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboTipo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnExecutar;
        private System.Windows.Forms.ProgressBar barraProgresso;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelTipo;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox comboTipo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown numericLoja;
        private ComponentFactory.Krypton.Toolkit.KryptonRichTextBox labelExecucao;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox labelSenha;
        private ComponentFactory.Krypton.Toolkit.KryptonButton botaoSenha;
    }
}

